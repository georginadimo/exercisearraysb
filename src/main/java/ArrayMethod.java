import lombok.NoArgsConstructor;

import java.util.Arrays;

@NoArgsConstructor
public class ArrayMethod {
    private boolean notfound=false;
    // Function to find the index of an element in a primitive array in Java
    public int findLinearSearch(int[] a, int target)
    {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == target) {
                System.out.println("Target found at index " + i + " of the array");
                notfound=true;
            }
        }
        if(notfound==false){
            System.out.println("Target not Found!");
        }
      return -1;


    }

    // Function to find the index of an element in a primitive array in Java
    public static int findBinarySearch(int[] a, int target)
    {
        int index = Arrays.binarySearch(a, target);

        return (index < 0) ? -1 : index;
    }

}
